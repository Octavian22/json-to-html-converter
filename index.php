<?php

declare(strict_types=1);

require_once __DIR__ . '/autoload.php';

$jsonToHtmlConverter = new JsonToHtmlCompositeConverter(
    file_get_contents('data.json', true)
);

$htmlComposite = $jsonToHtmlConverter->get();

echo $htmlComposite->render();