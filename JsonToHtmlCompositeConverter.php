<?php

declare(strict_types=1);

use HtmlComposite\Elements\BlockElement;
use HtmlComposite\Elements\ButtonElement;
use HtmlComposite\Elements\ContainerElement;
use HtmlComposite\Elements\ImageElement;
use HtmlComposite\Elements\TextElement;

class JsonToHtmlCompositeConverter
{
    private array $jsonArray;

    public function __construct(
        private readonly string $json,
    )
    {
        $this->jsonArray = json_decode($this->json, true);
    }

    public function get(): \HtmlComposite\HtmlComposite
    {
        return $this->build($this->jsonArray);
    }

    private function build(array $html): \HtmlComposite\HtmlComposite|\HtmlComposite\Element
    {
        if (!array_key_exists('children', $html)) {
            $html['children'] = [];
        }
        if (count($html['children']) > 0) {
            /** @var \HtmlComposite\HtmlComposite $htmlComposite */
            $htmlComposite = $this->assignClass($html);
            foreach ($html['children'] as $child) {
                $htmlComposite->setChildren($this->build($child));
            }
        } else {
            return $this->assignClass($html);
        }
        return $htmlComposite;
    }

    /**
     * @throws Exception
     */
    private function assignClass(array $html): \HtmlComposite\Element
    {
        return match ($html['type']) {
            'container' => new ContainerElement(...$html),
            'block' => new BlockElement(...$html),
            'text' => new TextElement(...$html),
            'image' => new ImageElement(...$html),
            'button' => new ButtonElement(...$html),
            'default' => throw new Exception('Not found Html element: ' . json_encode($html)),
        };
    }
}