<?php

declare(strict_types=1);

namespace HtmlComposite;

abstract class Element
{
    public function __construct(
        protected string $type,
        protected array $payload,
        protected array $parameters,
        protected array $children
    ) {}

    abstract public function render(): string;
}