<?php

declare(strict_types=1);

namespace HtmlComposite\Elements;

use HtmlComposite\Element;

class ButtonElement extends Element
{
    private ?string $url;

    private ?string $text;

    private string $styles;

    public function __construct(string $type, array $payload, array $parameters, array $children)
    {
        parent::__construct($type, $payload, $parameters, $children);
        $this->url = isset($this->payload['link']) ? $this->payload['link']['payload'] : null;
        $this->text = $payload['text'] ?? null;
        $this->styles = implode('; ', $this->parameters);
    }

    public function render(): string
    {
        return "<button style='$this->styles'><a href='$this->url'>$this->text</a></button>";
    }
}