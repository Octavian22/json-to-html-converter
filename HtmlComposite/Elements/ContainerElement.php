<?php

declare(strict_types=1);

namespace HtmlComposite\Elements;

use HtmlComposite\HtmlComposite;

class ContainerElement extends HtmlComposite
{
    public function render(): string
    {
        $childrens = parent::render();
        return "<div class='container'>$childrens</div>";
    }
}