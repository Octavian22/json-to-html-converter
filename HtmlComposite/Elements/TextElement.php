<?php

declare(strict_types=1);

namespace HtmlComposite\Elements;

use HtmlComposite\Element;

class TextElement extends Element
{
    public function render(): string
    {
        $text = $this->payload['text'];
        $style = implode('; ', $this->parameters);
        return "<p style='$style'>$text</p>";
    }
}