<?php

declare(strict_types=1);

namespace HtmlComposite\Elements;

use HtmlComposite\Element;

class ImageElement extends Element
{
    private string $styles;
    private string $src;
    private int $width;
    private int $height;

    public function __construct(string $type, array $payload, array $parameters, array $children)
    {
        parent::__construct($type, $payload, $parameters, $children);
        $this->styles = implode($this->parameters);
        $this->src = $this->payload['image']['url'];
        $this->width = $this->payload['image']['meta']['width'];
        $this->height = $this->payload['image']['meta']['height'];
    }

    public function render(): string
    {
        return "<img src='$this->src' style='$this->styles' width='$this->width' height='$this->height' alt=''>";
    }
}