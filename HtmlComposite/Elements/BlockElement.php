<?php

declare(strict_types=1);

namespace HtmlComposite\Elements;

use HtmlComposite\HtmlComposite;

class BlockElement extends HtmlComposite
{
    public function render(): string
    {
        $styles = implode('; ', $this->parameters);
        $childrens = parent::render();
        return "<div class='block' style='$styles'>$childrens</div>";
    }
}