<?php

declare(strict_types=1);

namespace HtmlComposite;

abstract class HtmlComposite extends Element
{
    /**
     * @var Element[]
     */
    protected array $fields = [];

    public function setChildren(Element $children): void
    {
        $this->fields[] = $children;
    }

    public function getChildren(): array
    {
        return $this->fields;
    }

    public function render(): string
    {
        $outputHtml = '';
        foreach ($this->fields as $field) {
            $outputHtml .= $field->render();
        }
        return $outputHtml;
    }
}